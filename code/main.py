#!../venv/bin/python3
# -*-coding:UTF-8 -*

#Importation modules générales
import os
import getpass
import paramiko
import requests
from scp import SCPClient
import nmap
import sys


def getIdPass():
    """
        Methode pour recuperer les id et passwd
    """
    ident = input("Entrer le user : ")
    mdp = getpass.getpass("Entre votre mdp : ") 
    return ident, mdp


def copyFile(host, ident, mdp):
    """
        Methode pour ancer une connexion ssh et transferer un fichier
    """
    ssh_client=paramiko.SSHClient()
    ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh_client.connect(hostname=host,username=ident,password=mdp)

    #On recupere le fichier python
    url = 'https://raw.githubusercontent.com/vulmon/Vulmap/master/Vulmap-Linux/vulmap-linux.py'
    r = requests.get(url, allow_redirects=True)
    open('vulmap-linux.py', 'wb').write(r.content)

    #On copie le fichier sur le serveur distant
    scp = SCPClient(ssh_client.get_transport())
    scp.put('vulmap-linux.py', 'vulmap-linux.py')

    #On execute le mot de passe sur la machine
    stdin, stdout, stderr = ssh_client.exec_command("python3 vulmap-linux.py")
    f = open('results', 'w')
    f.write("\n\n\n************\n* VULMAP LINUX\n************\n")
    for line in stdout.readlines():
        f.write(line)
    f.close()


def scan(ipcible):
    """

    """
    print("Debut du scan sur {}".format(ipcible))
    f = open('results', 'a')
    
    nm = nmap.PortScanner()
    
    f.write("\n\n\n************\n* NMAP SCAN PORT\n************\n")
    nm.scan(ipcible,  arguments='-O')
    
    #Scan des port
    for proto  in nm[ipcible].all_protocols():
        print('----------')
        f.write('----------')
        print('Protocol : %s' % proto)
        f.write('Protocol : %s' % proto)

        lport = nm[ipcible][proto].keys()
        for port in lport:
            print ('port : %s\tstate : %s' % (port, nm[ipcible][proto][port]['state']))
            f.write('port : %s\tstate : %s' % (port, nm[ipcible][proto][port]['state']))

    #Scan os
    f.write("\n\n\n************\n* NMAP SCAN OS\n************\n")
    if 'osclass' in nm[ipcible]:
        for osclass in nm[ipcible]['osclass']:
            print('OsClass.type : {0}'.format(osclass['type']))
            print('OsClass.vendor : {0}'.format(osclass['vendor']))
            print('OsClass.osfamily : {0}'.format(osclass['osfamily']))
            print('OsClass.osgen : {0}'.format(osclass['osgen']))
            print('OsClass.accuracy : {0}'.format(osclass['accuracy']))
            print('')
            f.write('OsClass.type : {0}'.format(osclass['type']))
            f.write('OsClass.vendor : {0}'.format(osclass['vendor']))
            f.write('OsClass.osfamily : {0}'.format(osclass['osfamily']))
            f.write('OsClass.osgen : {0}'.format(osclass['osgen']))
            f.write('OsClass.accuracy : {0}'.format(osclass['accuracy']))
            f.write('')
    

    #Scan vuln with nse
    a = nm.scan(ipcible, arguments='--script=/home/chris/00_Eagle/11_Eval/app/01_Python_App/vulscan.nse')
    f.write("\n\n\n************\n* NMAP SCAN VULN\n************\n")
    f.write(str(a["scan"]).replace("{", "{\n"))

    #Fermeture du fichier
    f.close()


if __name__ == "__main__":

    
    ipcible = "192.168.0.20"
    ipssh = "192.168.0.20"
    #On demande si identifiant
    yesno = input("Avez-vous des ientifiants sur la machine (yes/no) : ")
    
    if yesno == "yes":
        #On recupere les identifiant
        ident, mdp = getIdPass()

        #On copie colle le fichier vulmap
        copyFile(ipssh, ident, mdp)

    #On fait le scan sur la machine cible
    scan(ipcible)

    #On quitte
    print("Fin du programme")
    sys.exit(0)



